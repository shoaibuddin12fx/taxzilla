import { ChangeDetectorRef, Component } from '@angular/core';
import { IAPProduct, InAppPurchase2 } from '@ionic-native/in-app-purchase-2/ngx';
import { InAppPurchase } from '@ionic-native/in-app-purchase/ngx';
import { Platform, AlertController } from '@ionic/angular';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  products = [];
  productIDs = ['com.dev.taxzilla.yearly', 'com.dev.taxzilla.month'];

  constructor(
    private plt: Platform,
    private store: InAppPurchase2,
    private iap: InAppPurchase,
    private alertController: AlertController,
    private ref: ChangeDetectorRef
  ) {

    this.initialize();

  }

  async initialize(){

    await this.plt.ready();

    // this.iap
    // .getProducts(this.productIDs)
    // .then((products) => {
    //   console.log(products);
    //   this.products = products;
    //   //  [{ productId: 'com.yourapp.prod1', 'title': '...', description: '...', price: '...' }, ...]
    // })
    // .catch((err) => {
    //   console.log(err);
    // });

    this.store.ready.bind( () => {
      console.log('hot');
    });

    console.log(this.store);


    this.products = this.store.products;
    console.log(this.products);
    this.products.forEach( item => {
      // this.registerHandlersForPurchase(item.id);
    });
    this.ref.detectChanges();
    // this.registerHandlersForPurchase(productId)

  }

  buyProducts(product) {

    this.purchase(product);
    // this.iap
    //   .buy(productId)
    //   .then((data) => {
    //     console.log(data);
    //     alert('Purchase was successful!');
    //   }, err => {
    //     console.log(err);
    //   })
    //   .catch((err) => {
    //     console.log(err);
    //     alert('Something went wrong');
    //   });
  }

  restorePurchase() {
    this.iap
      .restorePurchases()
      .then((data) => {
        console.log(data);
        alert('Purchase was successful!');
      })
      .catch((err) => {
        console.log(err);
        alert('Something went wrong');
      });
  }

  // registerProducts() {
  //   this.store.register({
  //     id: PRODUCT_GEMS_KEY,
  //     type: this.store.CONSUMABLE,
  //   });

  //   this.store.register({
  //     id: PRODUCT_PRO_KEY,
  //     type: this.store.NON_CONSUMABLE,
  //   });

  //   this.store.refresh();
  // }

  // setupListeners() {
  //   // General query to all products
  //   this.store.when('product')
  //     .approved((p: IAPProduct) => {
  //       // Handle the product deliverable
  //       if (p.id === PRODUCT_PRO_KEY) {
  //         this.isPro = true;
  //       } else if (p.id === PRODUCT_GEMS_KEY) {
  //         this.gems += 100;
  //       }
  //       this.ref.detectChanges();

  //       return p.verify();
  //     })
  //     .verified((p: IAPProduct) => p.finish());


  //   // Specific query for one ID
  //   this.store.when(PRODUCT_PRO_KEY).owned((p: IAPProduct) => {
  //     this.isPro = true;
  //   });
  // }

  // registerHandlersForPurchase(productId) {

  //   let self = this.store;
  //   this.store.when(productId).updated(product => {
  //     if (product.loaded && product.valid && product.state === self.APPROVED && product.transaction != null) {
  //       product.finish();
  //     }
  //   });

  //   this.store.when(productId).registered((product: IAPProduct) => {
  //     alert(` owned ${product.owned}`);
  //   });
  //   this.store.when(productId).owned((product: IAPProduct) => {
  //     alert(` owned ${product.owned}`);
  //     product.finish();
  //   });
  //   this.store.when(productId).approved((product: IAPProduct) => {
  //     alert('approved');
  //     product.finish();
  //   });
  //   this.store.when(productId).refunded((product: IAPProduct) => {
  //     alert('refunded');
  //   });
  //   this.store.when(productId).expired((product: IAPProduct) => {
  //     alert('expired');
  //   });
  // }

  purchase(product) {

    console.log('clicked', product);

    try{

      // this.iap
      // .buy(product.productId)
      // .then((data) => {
      //   console.log(data);
      //   alert('Purchase was successful!');
      // })
      // .catch((err) => {
      //   console.log(err);
      //   alert('Something went wrong');
      // });



      let produc = this.store.get(product.id);
      console.log('JSION', produc);
      this.store.order(product.id).then(p => {
        // Purchase in progress!
        console.log("product purchase", p);
        console.log(p);
      }, e => {
        console.log(e);
        // this.presentAlert('Failed', `Failed to purchase: ${e}`);
      });

    } catch(e){
      console.log("ERROR", e);
    }

  }

  // // To comply with AppStore rules
  // restore() {
  //   this.store.refresh();
  // }

  // async presentAlert(header, message) {
  //   const alert = await this.alertController.create({
  //     header,
  //     message,
  //     buttons: ['OK']
  //   });

  //   await alert.present();
  // }


}
