import { Component } from '@angular/core';
import { InAppPurchase2 } from '@ionic-native/in-app-purchase-2/ngx';
import { InAppPurchase } from '@ionic-native/in-app-purchase/ngx';
import { Platform } from '@ionic/angular';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {

  productIDs = ['com.dev.taxzilla.yearly', 'com.dev.taxzilla.month'];

  constructor(private platform: Platform,
    private inAppPurchase: InAppPurchase2) {

    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      // this.inAppPurchase.verbosity = this.inAppPurchase.DEBUG;

      // const doSet = localStorage.getItem('doset');
      // if(!doSet){
        this.refreshAppProducts();
        // localStorage.setItem('doset', 'doset');
      // }

    });
  }
  private refreshAppProducts() {

    this.productIDs.forEach(productId => {
      this.inAppPurchase.register({
        id: productId,
        type: this.inAppPurchase.CONSUMABLE,
        alias: productId
      });
    });
    this.inAppPurchase.refresh();


  }

}
